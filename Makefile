###############################################################################
#         University of Hawaii, College of Engineering
# @brief  Lab 06c - Countdown - EE 205 - Spr 2022
#
# @file    Makefile
# @version 1.0
#
# @author Baishen Wang<baishen@hawaii.edu>
# @date   20_Feb_2022
#
# @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

TARGET = countdown


all:  $(TARGET)


CC     = gcc
CFLAGS = -Wall -Wextra $(DEBUG_FLAGS)


debug: DEBUG_FLAGS = -g -DDEBUG
debug: clean $(TARGET)


countdown.o: countdown.c
	$(CC) $(CFLAGS) -c countdown.c

countdown.o: countdown.c countdown.h
	$(CC) $(CFLAGS) -o $(TARGET) countdown.o


test: $(TARGET)
	./$(TARGET)


clean:
	rm -f $(TARGET) *.o
