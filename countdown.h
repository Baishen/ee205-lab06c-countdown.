///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// @author Baishen Wang <baishen@hawaii.edu>
// @date   20_Feb_2022
///////////////////////////////////////////////////////////////////////////////
const int refrenceYear = 2011;
const int refrenceMonth = 5;
const int refrenceDayOfMonth = 4;
const int refrenceHour = 4;
const int refrenceMinute = 0;
const int refrenceSecond = 0;
const int isDaylightSavings = -1;

const int hstOffset = 10;

struct tm refrenceTime;

